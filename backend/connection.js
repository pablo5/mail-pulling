const mysql = require('mysql');

class connection {
  constructor() {
    this.pool = null;
    this.start = function () {
      this.pool = mysql.createPool({
        connectionLimit: 100,
        host: 'localhost',
        user: 'root',
        password: '',
        database: 'test_mail'
      });
    };
    this.getConnection = callback => {
      this.pool.getConnection(function (error, connection) {
        callback(error, connection);
      });
    };
  }
}

module.exports = new connection();