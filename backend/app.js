process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

const express = require('express');
const cors = require('cors');
const app = express();
const connection = require('./connection');
const { getMailsFromIMAPServer } = require('./imap_server');
const { getMailsInfo } = require('./models/model');

connection.start();

getMailsFromIMAPServer();

app.use(cors());

app.get('/api/mails', (_, response) => getMailsInfo(response));

app.post('/api/mails/refresh', (_, response) => {
  getMailsFromIMAPServer();
  response.send({status: 'OK', data: 'Refreshing data'})
});

const server = app.listen(8081, () => {
  console.log('Running in port', server.address().port);
});