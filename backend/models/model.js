const connection = require('../connection');

module.exports = {
  saveMailInfo: mailInfo => {
    try {
      connection.getConnection((er, cn) => {
        const sql = `INSERT IGNORE INTO mails (Id, recieved_date, subject, from_user)
        VALUES ('${mailInfo.id}', '${mailInfo.date}', '${mailInfo.subject}', '${mailInfo.from}')`;
        cn.query(sql, (error, _) => {
          cn.release();
          if (error) {
            console.log('Error trying to save email info: ' + error);
          }
        })
      })
    } catch (error) {
      console.log('Error trying to save email info: ' + error);
    }
  },
  getMailsInfo: response => {
    try {
      connection.getConnection((er, cn) => {
        const sql = 'SELECT * FROM mails';
        cn.query(sql, (error, result) => {
          cn.release();
          if (error) {
            console.log('Cannot get mails info: ' + error);
            response.send({ status: 'Error', data: 'Cannot get mails info' })
          } else {;
            response.send({ status: 'OK', data: result })
          }
        })
      })
    } catch (error) {
      console.log('Error trying to get mails info: ' + error);
    }
  },
}