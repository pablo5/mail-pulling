const Imap = require('imap');
const MailParser = require('mailparser').MailParser;
const { saveMailInfo } = require('./models/model');
const config = require('./config/config.json')

const DEVOPS = 'DevOps';

const imap = new Imap({
  user: config.user_mail,
  password: config.user_password,
  host: 'imap.gmail.com',
  port: 993,
  tls: true
});

function getMailsFromIMAPServer() {
  const openInbox = callBack =>
    imap.openBox('INBOX', true, callBack);

  const processMessage = (msg, seqno) => {
    const parser = new MailParser();
    const mailInfo = {
      id: '',
      date: '',
      from: '',
      subject: '',
      body: ''
    };

    parser.on('headers', headers => {
      mailInfo.date = headers.get('date');
      mailInfo.from = headers.get('from').text;
      mailInfo.subject = headers.get('subject');
    });

    parser.on('data', data => {
      if (data.type === 'text') {
        mailInfo.id = seqno;
        mailInfo.body = data.text.trim();
      }
    });

    parser.on('end', () => {
      if (mailInfo.subject.includes(DEVOPS) || mailInfo.body.includes(DEVOPS)) {
        saveMailInfo(mailInfo);
      }
    })

    msg.on('body', stream => {
      stream.on('data', chunk => {
        parser.write(chunk.toString('utf8'));
      });
    });

    msg.once('end', () => parser.end());
  }

  imap.once('ready', () => {
    openInbox((err, box) => {
      if (err) {
        console.error('Error trying to get mail list');
        throw err;
      }

      const imapData = imap.seq.fetch(`1:${box.messages.total}`, { bodies: '' });

      imapData.on('message', processMessage);

      imapData.once('error', err => {
        console.log('Fetch error: ' + err);
      });

      imapData.once('end', () => imap.end());
    });
  });

  imap.once('error', err => console.log(err));

  imap.once('end', () => imap.closeBox());

  imap.connect();
}

module.exports = {
  getMailsFromIMAPServer
};