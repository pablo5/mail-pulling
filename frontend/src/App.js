import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

const getInstance = () => axios.create({
  baseURL: 'http://localhost:8081/api/',
  timeout: 1000,
});

const useStyles = makeStyles({
  table: {
    minWidth: 650,
  },
});

const App = () => {
  const classes = useStyles();
  const [rows, setRows] = useState([]);

  useEffect(() => {
    try {
      getRows()
    } catch (error) {
      throw error;
    }
  }, []);

  const getRows = () => {
    getInstance().get('/mails')
      .then(function (response) {
        setRows(response.data.data)
      })
  }

  const handleRefreshMails = () => {
    getInstance().post('/mails/refresh')
      .then(function (response) {
        alert(response.data.data);
      })
  }
  return (
    <div style={{ height: 400, width: '100%' }}>
      <TableContainer component={Paper}>
        <Table className={classes.table} size="small" aria-label="a dense table">
          <TableHead>
            <TableRow>
              <TableCell>Id</TableCell>
              <TableCell align="right">Date</TableCell>
              <TableCell align="right">Subject</TableCell>
              <TableCell align="right">From</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {rows.map(row => (
              <TableRow key={row.Id}>
                <TableCell align="right">{row.Id}</TableCell>
                <TableCell align="right">{row.recieved_date}</TableCell>
                <TableCell align="right">{row.subject}</TableCell>
                <TableCell align="right">{row.from_user}</TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
      <Button variant="contained" onClick={ getRows }>Get Data</Button>
      <Button variant="contained" onClick={ handleRefreshMails }>Run pulling mails process</Button>
    </div>
  );
}

export default App;
