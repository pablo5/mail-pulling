# Mail Pulling

This project allows us to pull mails from a Gmail account and persist all of those
that has the key word "DevOps" in the subject or body message

There is a fake Gmail account to test the application. The account configuration is in a JSON file
in the config folder in the backend project.

If you want to use a different Gmail account you need to setup some configurations in your Gmail account
and replace with your credentials the config.json file
 
 * Enable IMAP access
 * Turn on access for less secure apps setting

## Pre requistes

 * Node JS installed
 * MySQL installed
  * Data base created with name test_mail
  * Apply script to create table (backend/scripts/CREATE_MAILS_TABLE.sql)
  
## Project structure

The project has two folders.

* The first one is an Express API that allow us to run a process that pull all the mails
that has the key word "DevOps" in the subject or body message.

  Also there is an end point to allow us to get the data to display in a React app

* The second one is related to the front end side, it is a basic project only for validate the data result

## Run project

### Start projects

  * In order to start both project, you need to complete the pre requisites steps then you need to run
  `npm install` in a command line in both project (backend and frontend) and 
  `npm start` to start the project

### Data visualization

  * To validate the data, after start the frontend project you will be able to open the browser in `localhost:3000`